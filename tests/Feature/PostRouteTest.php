<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use App\Models\Post;

class PostRouteTest extends TestCase
{
    use DatabaseMigrations;

    // TODO: 針對 API route 撰寫測試
    public function testGetPostrequest()
    {
        $response = $this->call('GET', 'api/posts');

        $response->assertStatus(200);

        $this->assertJsonStructure([
            '*' => [
                'name',
                'user_id',
                'content',
                'category',
                'published_at',
                'created_at',
                'updated_at'
            ],
        ], $response);
    }
}

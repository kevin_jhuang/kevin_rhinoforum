<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function getPosts(Request $request)
    {
        // TODO: 實作查詢貼文 API

        // 參數驗證
        $validator = Validator::make($request->all(), [
            'start' => 'nullable|date_format:"Y-m-d H:i:s"|before:end',
            'end' => 'nullable|date_format:"Y-m-d H:i:s"',
            'content' => 'nullable|string',
            'category' => 'nullable|string',
            'page' => 'nullable|integer',
            'pageSize' => 'nullable|integer',
        ], [
            'page.integer' => '頁數請輸入整數',
            'pageSize.integer' => '筆數請輸入整數',
            'content.string' => '內容請輸入字串',
            'category.string' => '分類請輸入字串',
            'marginTop.integer' => '與視窗最上方距離請輸入數字',
            'start.date_format' => '開始時間請輸入(yyyy-MM-dd HH:mm:ss)',
            'end.before' => '開始時間需小於結束時間',
            'end.date_format' => '結束時間請輸入(yyyy-MM-dd HH:mm:ss)',
        ]);

        if ($validator->fails()) {
            $errorMsg = $validator->errors()->first();
            return response()->json($errorMsg, 400);
        }

        // 初始分頁參數
        $request['page'] = $request['page']  ?? 1;
        $request['pageSize'] = $request['pageSize'] ?? 10;

        // Post 條件查詢
        $posts = Post::where(function ($query) use ($request) {
            if(isset($request['user_id']) && !is_null($request['user_id'])){
                $query->where('user_id', $request['user_id']);
            }
            // 分類
            if (isset($request['category']) && !is_null($request['category'])) {
                $query->where('category', $request['category']);
            }
            // 貼文內容
            if (isset($request['content']) && !is_null($request['content'])) {
                $query->where('content', 'like', "%{$request['content']}%");
            }
            // 發布時間
            if (isset($request['start'], $request['end']) && !is_null($request['start']) && !is_null($request['end'])) {
                $query->where('published_at', '>=', $request['start'])
                    ->where('published_at', '<=', $request['end']);
            }
        })->take($request['pageSize'])
        ->skip($request['page'] > 1 ? ($request['page'] - 1) * $request['pageSize'] : 0)
        ->get();

        return response()->json($posts);
    }
}
